# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions


## Setup working folder ##

mkdir -p ${HOME}/develop/yocto

YOCTODIR=${HOME}/develop/yocto

* if using a script

export $YOCTODIR

## Clone Bitbake ##

cd $YOCTODIR

git clone git://git.openembedded.org/bitbake

## Clone Git Repo ##

git clone git@bitbucket.org:devpro4/bb-nano.git

## Source setup env script ##

cd $YOCTODIR/bb-nano

source setup_env.sh

## Build ##

bitbake nano

## Verify Binary ##

nano binary is built in build sub-directory under

~/develop/yocto/bb-nano/tmp/work/**nano-2.7.2-r0/nano-2.7.2/build/src**

## Verify Bitbake Logs ##

~/develop/yocto/bb-nano/tmp/work/nano-2.7.2-r0/**temp**

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact