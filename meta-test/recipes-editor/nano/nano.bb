DESCRIPTION = "Recipe to build the 'nano' editor"

PN = "nano"
PV = "2.7.2"
PR = "r0"

SRC_URI = "https://www.nano-editor.org/dist/v2.7/nano-2.7.2.tar.gz"
SRC_URI[md5sum] = "8b880355f88bfa521d18dc6f18437095"
SRC_URI[sha256sum] = "89cc45dd630c6fb7276a14e1b3436a9972cf6d66eed15b14c3583af99070353c"

# we need to export these variable only in this minimal lab setup

export DL_DIR
export WORKDIR
export P

python do_fetch() {

  src_uri = (d.getVar('SRC_URI', True) or "").split()

  if len(src_uri) == 0:
    bb.fatal("Empty URI")
  
  bb.note("Downloading source tarball from ${SRC_URI} ... \n '%s' \n" % src_uri )

  try:
    fetcher = bb.fetch2.Fetch(src_uri, d)
    fetcher.download()
  except bb.fetch2.BBFetchException:
    bb.fatal("Could not fetch source tarball.")

  bb.note("Download successful")
}

python do_unpack() {

  bb.note ("Unpacking source tarball to ${WORKDIR} : '%s'..." % d.getVar('WORKDIR'))

  os.system("tar x -C ${WORKDIR} -f ${DL_DIR}/${P}.tar.gz")

  bb.note("Unpacked source tarball.")
}

python do_configure() {
  bb.note("Configuring source package ...")

  os.system("cd ${WORKDIR}/${P}; mkdir build; cd build && ../configure")

  bb.note("Configured source package.")
}

python do_compile() {
  bb.note("Compiling package '%s'" % (d.getVar('P', True)))

  os.system("cd ${WORKDIR}/${P}/build && make")

  bb.note("Compiler package.")

}

# Tasks

addtask fetch before do_build
addtask unpack before do_build after do_fetch
addtask configure before do_build after do_unpack
addtask compile before do_build after do_configure

